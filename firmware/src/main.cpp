
#include "main.h"

//custom
#include "wifi.h"
#include "config.h"
//esp
#include <FS.h>
//esp libs
#include <ESP8266WebServer.h>
//util
#include <Ticker.h>
#include <time.h>
#include <ArduinoJson.h>
#include <Adafruit_NeoPixel.h>


#ifdef ENV_DEV
#include <ArduinoOTA.h>
#endif


#define F_CONFIG "/config.json"
#define PIN_SWS_RX 14
#define PIN_SWS_TX 12
#define SWS_SPEED 9600
#define PIN_LED 13
#define PIN_MIC A0
#define PIXELS 1

#include "MSGEQ7.h"
#define pinAnalog A0
#define pinReset 4
#define pinStrobe 5
#define MSGEQ7_INTERVAL ReadsPerSecond(100)


CMSGEQ7<MSGEQ7_SMOOTH, pinReset, pinStrobe, pinAnalog> MSGEQ7;


#define BUF_CONFIG 256


#define I_CHECK_MIC_MS 5
#define I_STATUS_S 5
#define I_CHECK_WIFI_S 5
#define T_CONFIG_S 600
#define T_NO_WIFI_S 1200 //20m no wifi - reboot



Config config{
  "balloon", 
  "balL00n17988028"};



Ticker tickerStatus;
bool isCheckStatus;
bool isCheckMic;
Ticker tickerWifi;
Ticker tickerMic;
Ticker tickerConfigCancel;
Ticker tickerNoWifiReboot;
bool isCheckWifi;
bool isWifiWasConnected;
bool isWifiWasDisConnectedSinceBoot = true;
#define T_NO_WIFI_CONNECT_ON_BOOT_S 30

#define AP_SSID "eluminate-sol"
#define AP_PASS "0123456789"
char deviceName[24];


Adafruit_NeoPixel led = Adafruit_NeoPixel(PIXELS, PIN_LED, NEO_GRB + NEO_KHZ800);

enum ConnectStatus{
  CS_OK=0,
  CS_NO_WIFI=1,
  CS_PARSE_ERROR=3,
  CS_UNDEFINED=99
};


enum WsMode{
  WS_MODE_CONFIG=0,
  WS_MODE_COMMAND=1
};

ConnectStatus cs = ConnectStatus::CS_UNDEFINED;
WsMode wsMode = WsMode::WS_MODE_COMMAND;

#ifdef ENV_DEV
void initOta()
{
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    LOGF("%u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onStart([]() {
    });
    ArduinoOTA.onEnd([]() {          
    });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
      LOGF("%u ", (progress / (total / 100)));
    });
    ArduinoOTA.onError([](ota_error_t error) { 
    });
  ArduinoOTA.begin();
}
#endif


const String toString(ConnectStatus cs)
{
    switch (cs)
    {
        case CS_OK:   return "OK";
        case CS_NO_WIFI:   return "NO_WIFI";
        case CS_PARSE_ERROR: return "PARSE_ERROR";
        case CS_UNDEFINED: return "UNDEFINED";
        default:      return "[Unknown ConnectStatus]";
    }
}


void tickersAttachNetwork(){
  
  tickerWifi.attach(I_CHECK_WIFI_S, [](){isCheckWifi = true;});

}




void checkConnect(const Config &c)
{
  //stopDns(); //if dns run then wifi sta connect failed
  LOG(F("HttpServer::checkConnect"));  
  if(WiFi.isConnected()){
    WiFi.disconnect();
  }
  wifiSetSta(c);

  if (!wifiWaitForConnect(10000))
  {    
    cs = ConnectStatus::CS_NO_WIFI;
  }
  else
  {
    LOG(F("wifi ok"));
    cs = ConnectStatus::CS_OK;
  }
}

Config configTmp;

void configFromJson(Config &c, const JsonObject &root){
  LOG(F("configFromJson"));  
  strlcpy(c.wifiSsid, (const char*)root["wifiSsid"], sizeof(c.wifiSsid));
  strlcpy(c.wifiPass, (const char*)root["wifiPass"], sizeof(c.wifiPass));  
}



void configStore(){
  LOG(F("configStore"));

  DynamicJsonDocument doc(BUF_CONFIG);  
  doc["wifiSsid"] = config.wifiSsid;
  doc["wifiPass"] = config.wifiPass; 

  File file = SPIFFS.open(F_CONFIG, "w");

  if (!file) {
    LOG(F("Failed to open file for writing"));
    return;
  }

  if (serializeJson(doc, file) == 0) {
    LOG(F("Failed to write to file"));
  }  

  file.close();
  #ifdef ENV_DEV
  serializeJsonPretty(doc, Serial);
  #endif
}

bool configRestore()
{  
  LOG(F("configRestore"));
  File file = SPIFFS.open(F_CONFIG, "r");
  if (!file) {
    LOG("Failed to open file");
    return false;
  }


  DynamicJsonDocument doc(BUF_CONFIG);

  // Deserialize the JSON document
  DeserializationError error = deserializeJson(doc, file);   
  if (error){
    LOG(F("Failed to read file. json error"));
    return false;
  }
  #ifdef ENV_DEV
  serializeJsonPretty(doc, Serial);
  #endif
  JsonObject root = doc.as<JsonObject>();  
  configFromJson(config, root);

  file.close();

  return true;
}



void configApply(){
  LOG(F("configApply"));
  config = configTmp;
  wifiSta(config, deviceName);   
}


void reboot()
{
  LOG(F("Rebooting"));
  ESP.restart();
}



void configCancel(){  
  LOG(F("configCancel"));
  wifiSta(config, deviceName);
}


void configStart()
{
  LOG(F("configStart"));
  wifiApSta(config, AP_SSID, AP_PASS, deviceName);
  //wifiAp(AP_SSID, AP_PASS);
  tickerConfigCancel.once(T_CONFIG_S, configCancel);
}


void checkApInfo(){
  auto stat_info = wifi_softap_get_station_info();
  
  LOGF(" Total Connected Clients are = %i\n", wifi_softap_get_station_num());
  int i = 0;  
  bool isAny = false;
  while (stat_info != NULL) {    
    isAny = true;

    IPAddress ip(&stat_info->ip);
    
    LOG_(F("client= "));
    
    LOG_(i);
    LOG_(F(" IP adress is = "));
    LOG(ip.toString());

    stat_info = STAILQ_NEXT(stat_info, next);
    i++;
  }
}

void checkStatus(){
  LOG(F("Status:\n"));
  isCheckStatus = false;
  if(WiFi.isConnected()){
    LOGF("\tWiFi (%s): connected ip = %s\n", config.wifiSsid, WiFi.localIP().toString().c_str() );    
  }else{
    LOGF("\tWiFi (%s): disconnected\n", config.wifiSsid);
  }
  
  switch (WiFi.getMode())
  {
    case WIFI_AP:
      LOGF("\tWiFi mode: WIFI_AP: %s\n", WiFi.softAPSSID().c_str());
      checkApInfo();
      break;
    case WIFI_AP_STA:
      LOGF("\tWiFi mode: WIFI_AP_STA: %s\n", WiFi.softAPSSID().c_str());      
      checkApInfo();
      break;
    case WIFI_STA:
      LOG("\tWiFi mode: WIFI_STA");
      break;
    case WIFI_OFF:
      LOG("\tWiFi mode: WIFI_OFF");
      break;
  }  

  LOGF("\tUptime: %li s\n", millis()/1000);

  uint32_t free;
  uint16_t max;
  uint8_t frag;
  ESP.getHeapStats(&free, &max, &frag);

  LOGF("\tHeap free: %5d - max: %5d - frag: %3d%% <- \n", free, max, frag);  
}


void checkMic(){
  
  isCheckMic = false;
  
  auto min = 300;

  auto val = analogRead(PIN_MIC)-min; //124 - 1024
  int brightness = 255 * val / (1024-min);
  auto b = (uint8_t)(brightness>255?255:(brightness<=0?1:brightness));
  LOGF("Mic level %i brightness=%i b= %i\n", val, brightness, b);

  led.setBrightness(b);
  led.show();
}




void setup()
{
  pinMode(PIN_LED, OUTPUT);
  pinMode(PIN_MIC, INPUT);

  strlcpy(deviceName,
        ("elum-sol-" + String(ESP.getChipId(), HEX)).c_str(),
        sizeof(deviceName) 
  );  
  Serial.begin(115200);
#if defined(ENV_DEV)
  Serial.begin(115200);
  LOG(F("ENV_DEV"));  
  initOta();
#endif
  LOG(F("Booting.."));
  
  configTime(0, 0, "pool.ntp.org", "time.nist.gov");
  
  #if defined(ENV_DEV) || defined(DEBUG_HEAP)
  tickerStatus.attach(I_STATUS_S, [](){isCheckStatus = true;});
  #endif
  
  tickersAttachNetwork();
  
  
  tickerMic.attach_ms(I_CHECK_MIC_MS, [](){isCheckMic = true;});

  
  
  if (!SPIFFS.begin()) {
    LOG(F("Failed to mount file system"));    
  }else{
    FSInfo fsInfo;    
    SPIFFS.info(fsInfo);
    LOGF("FS info\n\ttotalBytes=%i\n\tusedBytes=%i\n\tblockSize=%i\n\tpageSize=%i\n\tmaxOpenFiles=%i\n\tmaxPathLength=%i\n",fsInfo.totalBytes,fsInfo.usedBytes,fsInfo.blockSize,fsInfo.pageSize,fsInfo.maxOpenFiles,fsInfo.maxPathLength);
  }

  configStore();

  if(configRestore()){
    wifiApSta(config, AP_SSID, AP_PASS, deviceName);   
  }else{
    wifiAp(AP_SSID, AP_PASS);   
  }
  

  configStart();

  isCheckWifi = true;

  led.begin();
  led.setPixelColor(0, 0,128, 255);
  led.setBrightness(255);
  led.show();

   MSGEQ7.begin();

}


void wifiCheck(){
  LOG_(F("wifiCheck.."));
  isCheckWifi = false; 
  auto  mode = WiFi.getMode();
  if(mode == WIFI_STA || mode == WIFI_AP_STA){
    LOGF("WIFI mode (STA - 1; AP_STA - 3 ): %i\n", mode);
    auto ssidActual = WiFi.SSID();
    bool connected = WiFi.isConnected();
    if(!ssidActual.equals(config.wifiSsid) && strlen(config.wifiSsid)>0){
      LOGF("WiFi ssid from config '%s' different from actual '%s'\nTry reinit wifi params\n",
       config.wifiSsid, 
       ssidActual.c_str());
      wifiSetSta(config);
    }else if(!connected){      
      LOGF("WiFi '%s' not connected\n", 
      ssidActual.c_str());      
      //wifi disconnected and wasconnected - start timer fo reboot
      if(isWifiWasConnected){
        LOG("Try reinit wifi params");      
        wifiSetSta(config);
        LOG(F("just disconnected"));
        isWifiWasConnected = false;
        tickerNoWifiReboot.once(T_NO_WIFI_S, reboot);
      }else{
        LOG(F("still disconnected"));        
      }
    } else if (!isWifiWasConnected){
      isWifiWasDisConnectedSinceBoot = false;
      LOG(F("ok - just connected"));
      //wifi connected and was disconnected - stop reboot timer
      isWifiWasConnected = true;
      //connect mqtt after wifi connect
      tickerNoWifiReboot.detach();
    } else {
      LOG(F("ok - still connected"));
    }

    if(!isWifiWasConnected 
        && (isWifiWasDisConnectedSinceBoot == true)
        && ((millis()/1000) > T_NO_WIFI_CONNECT_ON_BOOT_S))
    {
      LOGF("no STA connection %i s since boot.Set AP mode\n", T_NO_WIFI_CONNECT_ON_BOOT_S);
      wifiAp(AP_SSID, AP_PASS);
    }
  }
  if (mode == WIFI_AP_STA){
    LOG_(F("AP_STA mode.."));
    auto apSsidActual = WiFi.softAPSSID();  
    if(!apSsidActual.equals(AP_SSID)){
      LOGF("WiFi AP ssid '%s' different from actual '%s'\nTry reinit wifi params\n",
       AP_SSID, apSsidActual.c_str());
      wifiApSta(config, AP_SSID, AP_PASS, deviceName);
    }else{      
      LOG(F("ok"));
    }
  }
  else if (mode == WIFI_AP){
    LOG_(F("AP mode.."));
    auto apSsidActual = WiFi.softAPSSID();  
    if(!apSsidActual.equals(AP_SSID)){
      LOGF("WiFi AP ssid '%s' different from actual '%s'\nTry reinit wifi params\n",
       AP_SSID, apSsidActual.c_str());
      wifiAp(AP_SSID, AP_PASS);
    }else{      
      LOG(F("ok"));
    }
  }
}



void serialBars(uint16_t FPS) {
  // Visualize the average bass of both channels
  uint8_t input = MSGEQ7.get(MSGEQ7_MID);

  // Reduce noise
  input = mapNoise(input);

  auto b = (uint8_t)(input>255?255:(input<=0?1:input));
  led.setBrightness(b);
  led.show();
  // Save the difference between the last beat
  static uint8_t lastInput = 0;
  int delta = input - lastInput;
  lastInput = input;

  // All channels together
  Serial.print(input);
  Serial.print(F(" In\t"));

  // Difference between last measurement
  // Serial.print(delta);
  // Serial.print(F(" D\t"));

  // 1st channel (here: left)
  Serial.print(MSGEQ7.get(MSGEQ7_MID));
  Serial.print(F(" R\t"));

  // // overall volume of all channels
  // Serial.print(MSGEQ7.getVolume());
  // Serial.print(F(" Vol\t"));

  // FPS of the reading
  // Serial.print(FPS);
  // Serial.print(F(" FPS \t"));

  // Highlight high pitches
  char c = '=';
  if (delta >= 20)
    c = '#';

  // Visualize with characters as bars
  for (uint8_t i = 0; i < (input / 4); i++)
  {
    if (i == 192 / 4 - 1)
      Serial.print('+');
    else if (i == 128 / 4 - 1)
      Serial.print('*');
    else if (i == 64 / 4 - 1)
      Serial.print('X');
    else
      Serial.print(c);
  }
  Serial.println();
}

uint16_t getFPS(bool newReading) {
  // Variables to count FPS and last 1 second mark
  static uint16_t prevFPS = 0;
  static uint16_t FPS = 0;
  static uint32_t m = 0;

  // Increase FPS count
  if (newReading)
    FPS++;

  // If 1 second mark crossed, save new FPS
  if ((micros() - m) > 1000000) {
    prevFPS = FPS;
    FPS = 0;
    m = micros();
  }

  return prevFPS;
}


void loop()
{
#ifdef ENV_DEV
  ArduinoOTA.handle();
#endif

  if(isCheckWifi){ wifiCheck(); }
  if(isCheckStatus){ checkStatus(); }
 // if(isCheckMic){ checkMic(); }
  //checkMic();

 bool newReading = MSGEQ7.read(MSGEQ7_INTERVAL);

  // Calculate FPS
  //uint16_t FPS = getFPS(newReading);

    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    
  // Serial raw debug output
  if (newReading){    
    for(auto i = 0; i<7 ;i++){
      
      Serial.printf("%i: ", i);
      char c = '=';
      uint8_t input = MSGEQ7.get(i);
      
      input = mapNoise(input);
       for (uint8_t i = 0; i < (input / 4); i++)
        {
          if (i == 192 / 4 - 1)
            Serial.print('+');
          else if (i == 128 / 4 - 1)
            Serial.print('*');
          else if (i == 64 / 4 - 1)
            Serial.print('X');
          else
            Serial.print(c);
        }
        Serial.println();
    }
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println();
    

  }
    //serialBars(FPS);

  yield();
}
